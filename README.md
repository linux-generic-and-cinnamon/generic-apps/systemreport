## SystemReport

A **Python3** script that creates system report information based on data returned by the inxi tool.

Requires the installation of the **pyperclip** module, if not already installed.
For this, the **pip** package should already be installed.

Also, **inxi** is mandatory. Certain distros (i.e. Debian) do not have this package installed by default.

First run should be in terminal in order to notice any possible errors (such as missing packages), and correct them. Afterwards the script could be run from a shortcut/launcher/hotkey.

The output is automatically placed in the clipboard, including BBCode `[code]` tags for posting on forum boards. The script displays notifications before building the report and after it was copied to clipboard.

Enjoy!

**© Drugwash, 2022-2023**
