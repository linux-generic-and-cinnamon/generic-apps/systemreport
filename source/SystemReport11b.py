#!/usr/bin/python3
## create system report
## © Drugwash 2022-2023 GPL v2+
## v 1.1b

import subprocess, sys
if sys.version_info[1] < 6:
# Python 3.4+ ##################
	import importlib
	isPpc = importlib.util.find_spec("pyperclip")
	if isPpc is None:
		subprocess.check_output("pip install pyperclip", shell=True)
	import pyperclip
else:
# Python 3.6+ ##################
	try:
		import pyperclip
	except ModuleNotFoundError:
		subprocess.check_output("pip install pyperclip", shell=True)
		import pyperclip
# common code ##################
cmnd = "notify-send 'System report' 'please wait...' -t 3 -u 'normal'"
subprocess.check_output(cmnd, shell=True)
cmnd = "inxi -Fxxxmpz --usb"
report = subprocess.check_output(cmnd, shell=True).decode()
pyperclip.copy("[code]"+report+"[/code]")
cmnd = "notify-send 'System report' 'information copied to clipboard' -t 4 -u 'normal'"
subprocess.check_output(cmnd, shell=True)
